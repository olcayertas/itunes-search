//
// Created by Olcay Ertaş on 19.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import os.log
import Combine
import UIKit
import SwiftUI

public extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!
    static let searchViewModel = OSLog(subsystem: subsystem, category: "ItunesSearchViewModel")
    static let baseItemViewModel = OSLog(subsystem: subsystem, category: "BaseItemViewModel")
    static let itemDetailViewModel = OSLog(subsystem: subsystem, category: "ItemDetailViewModel")
    static let itemViewModel = OSLog(subsystem: subsystem, category: "ItemViewModel")
    static let uiImageCache = OSLog(subsystem: subsystem, category: "UIImageCache")
    static let loadArray = OSLog(subsystem: subsystem, category: "LoadArray")
    static let baseApiClient = OSLog(subsystem: subsystem, category: "BaseApiClient")
    static let historyData = OSLog(subsystem: subsystem, category: "HistoryData")
}

extension Notification {
    var keyboardHeight: CGFloat {
        (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
    }
}

extension Publishers {
    static var keyboardHeight: AnyPublisher<CGFloat, Never> {
        let willShow = NotificationCenter.default.publisher(for: UIApplication.keyboardWillShowNotification).map { $0.keyboardHeight }
        let willHide = NotificationCenter.default.publisher(for: UIApplication.keyboardWillHideNotification).map { _ in CGFloat(0) }
        return MergeMany(willShow, willHide).eraseToAnyPublisher()
    }
}

extension View {
    func keyboardAdaptive() -> some View {
        ModifiedContent(content: self, modifier: KeyboardAdaptive())
    }
}
