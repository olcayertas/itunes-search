//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine

extension BaseApiClient where Self == SearchApiClient {

    internal func fetchAndDecode<T: Codable>(_ components: URLComponents, type: T.Type) -> AnyPublisher<T, Error> {
        guard let url = components.url else {
            return Fail(error: ApiClientError.failedToBuildUrlFromComponents).eraseToAnyPublisher()
        }
        return fetch(url: url)
                .tryMap { (data, code) -> Data in
                    data
                }
                .decode(type: T.self, decoder: JSONDecoder())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
    }
}

struct SearchApiClient: BaseApiClient {

    func getResults(
            for query: String,
            media: MediaType = .all,
            entity: String,
            country: String = "tr",
            limit: Int = 100
    ) -> AnyPublisher<SearchResponse, Error> {
        var components = self.components
        components.path = "/search"
        components.queryItems = [
            URLQueryItem(name: "term", value: query),
            URLQueryItem(name: "media", value: media.rawValue),
            URLQueryItem(name: "entity", value: entity),
            URLQueryItem(name: "country", value: country),
            URLQueryItem(name: "limit", value: "\(limit)")
        ]
        return fetchAndDecode(components, type: SearchResponse.self)
    }

    func getResults(for queryParams: [String: String]) -> AnyPublisher<SearchResponse, Error> {
        var components = self.components
        components.path = "/search"
        components.queryItems = queryParams.map { key, value -> URLQueryItem in
            URLQueryItem(name: key, value: value)
        }
        return fetchAndDecode(components, type: SearchResponse.self)
    }
}

