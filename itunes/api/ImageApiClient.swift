//
// Created by Olcay Ertaş on 19.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine

struct ImageApiClient: BaseApiClient {

    func getImage(url: String) -> AnyPublisher<(Data, Int), Error> {
        guard let _url = URL(string: url) else {
            return Fail(error: ApiClientError.failedToCreateUrl(url)).eraseToAnyPublisher()
        }
        return fetch(url: _url)
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
    }
}