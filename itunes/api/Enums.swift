//
// Created by Olcay Ertaş on 19.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

public enum MediaType: String {
    case movie
    case podcast
    case music
    case musicVideo
    case audioBook = "audiobook"
    case shortFilm
    case tvShow
    case software
    case eBook = "ebook"
    case all
}

public enum EntityType {

    public enum MovieEntity: String {
        case movie
        case movieArtist
    }

    enum PodcastEntity: String {
        case podcast
        case podcastAuthor
    }

    enum MusicVideoEntity: String {
        case musicArtist
        case musicVideo
    }

    enum AudioBookEntity: String {
        case audioBookAuthor = "audiobookAuthor"
        case audioBook = "audiobook"
    }

    enum ShortFilmEntity: String {
        case shortFilm
        case shortFilmArtist
    }

    enum TVShowEntity: String {
        case tvEpisode
        case tvSeason
    }

    enum SoftwareEntity: String {
        case software
        case iPadSoftware
        case macSoftware
    }

    enum EBookEntity: String {
        case ebBook = "ebook"
    }

    enum MusicEntity: String {
        case album
        case musicVideo
        case musicArtist
        case musicTrack //Can contain both songs and music videos
        case mix
        case song
    }

    public enum AllEntity: String {
        case all
    }
}