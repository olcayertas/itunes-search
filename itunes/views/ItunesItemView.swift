//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import os.log


struct ItunesItemView: View {

    @ObservedObject private var viewModel = ItunesItemViewModel()

    init() {
        viewModel.item = .constant(ItunesItem())
        viewModel.getImage()
    }

    init(item: Binding<ItunesItem>) {
        viewModel.item = item
        viewModel.getImage()
    }

    func renderTexts() -> some View {
        VStack(alignment: .leading, spacing: 4) {
            Text(viewModel.trackName).lineLimit(1).font(.headline)
            Text(viewModel.artistName).lineLimit(1).font(.body)
            Text("Collection: \(viewModel.collectionName)").lineLimit(1).font(.body)
        }
    }

    func renderImage() -> some View {
        let data = $viewModel.imageData
        let kind = viewModel.kind
        return ItemImageView(imageData: data, kind: kind).frame(width: 84, height: 84).padding(8)
    }

    var body: some View {
        let background: Color = viewModel.item?.isVisited.wrappedValue == true ? .gray : .white
        let tint: Color = viewModel.item?.isVisited.wrappedValue == true ? .white : .blue
        return GeometryReader { geometry in
            HStack(alignment: .center, spacing: 16) {
                self.renderImage()
                self.renderTexts()
                Spacer()
                Button(action: {
                    self.viewModel.play()
                }) {
                    Image(systemName: self.$viewModel.playerIcon.wrappedValue).resizable().frame(width: 36, height: 36)
                }.padding(.trailing, 16).accentColor(tint)
            }.background(background)
        }
    }
}

struct ItunesItemView_Previews: PreviewProvider {
    static var previews: some View {
        ItunesItemView(item: .constant(ItunesItem()))
    }
}
