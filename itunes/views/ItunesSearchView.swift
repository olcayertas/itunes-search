//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import os.log
import UIKit


struct ItunesSearchView: View {

    private let wilResignPublisher = NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)
    private let wilTerminatePublisher = NotificationCenter.default.publisher(for: UIApplication.willTerminateNotification)

    @ObservedObject private var viewModel = ItunesSearchViewModel()
    @State private var keyboardHeight: CGFloat = 0
    @State var selectedItem = ItunesItem()
    @State var goToDetail = false

    func makeItemDetailView(_ item: Binding<ItunesItem>) -> some View {
        ItemDetailView(item: item) { item in
            withAnimation(.easeInOut(duration: 0.6)) {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if let index = self.viewModel.results.firstIndex(of: item) {
                        self.viewModel.results.remove(at: index)
                    }
                }
            }
        }
    }

    func makeItemView(_ item: Binding<ItunesItem>, _ width: CGFloat) -> some View {
        ItunesItemView(item: item).frame(width: width, height: 100)
    }

    func isPortrait(_ size: CGSize) -> Bool {
        size.width < size.height
    }

    func searchBarTopPadding(_ size: CGSize) -> CGFloat {
        isPortrait(size) ? 8 : 16
    }

    func columns(_ size: CGSize) -> Tracks {
        isPortrait(size) ? 1 : 2
    }

    func rows(_ size: CGSize) -> Tracks {
        let count: CGFloat = CGFloat(viewModel.results.count)
        let half = count / 2
        let count2 = viewModel.results.count % 2 == 0 ? half : half + 1
        return isPortrait(size) ? .fixed(count) : .fixed(count2)
    }

    func width(_ size: CGSize) -> CGFloat {
        isPortrait(size) ? size.width - 8 : (size.width - 12) / 2
    }
    
    func renderNavigationLink() -> some View {
        NavigationLink(destination: self.makeItemDetailView(self.$selectedItem), isActive: self.$goToDetail) {
            EmptyView()
        }
    }

    var body: some View {
        GeometryReader { geometry in
            NavigationView {
                VStack(alignment: .center) {
                    self.renderNavigationLink()
                    SearchBar(text: self.$viewModel.searchText).padding(.top, self.searchBarTopPadding(geometry.size))
                    LoadingView(isShowing: self.$viewModel.isLoading) {
                        ScrollView {
                            withAnimation {
                                Grid(0..<self.viewModel.results.count) { index in
                                    Button(action: {
                                        self.selectedItem = self.viewModel.results[index]
                                        self.goToDetail = true
                                    }, label: {
                                        self.makeItemView(self.$viewModel.results[index], self.width(geometry.size))
                                    }).buttonStyle(PlainButtonStyle())
                                            .frame(minHeight: 100)
                                            .animation(.easeInOut(duration: 0.5))
                                            .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
                                }
                                        .animation(.easeInOut(duration: 0.5))
                                        .transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
                            }
                        }.gridStyle(
                                ModularGridStyle(
                                        columns: .fixed(self.isPortrait(geometry.size) ? geometry.size.width : geometry.size.width / 2 - 8),
                                        rows: .fixed(100),
                                        spacing: 8
                                )
                        )
                    }
                }.navigationBarTitle("iTunes Search", displayMode: .inline).keyboardAdaptive()
                        .onReceive(self.wilResignPublisher) { _ in
                            self.viewModel.persist()
                        }
                        .onReceive(self.wilTerminatePublisher) { _ in
                            self.viewModel.persist()
                        }
            }.navigationViewStyle(StackNavigationViewStyle())
        }
    }
}

struct ItunesSearchView_Previews: PreviewProvider {
    static var previews: some View {
        ItunesSearchView()
    }
}
