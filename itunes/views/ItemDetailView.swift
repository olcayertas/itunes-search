//
// Created by Olcay Ertaş on 20.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import AVFoundation

struct ItemDetailView: View {

    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    @Environment(\.horizontalSizeClass) var horizontalSizeClass: UserInterfaceSizeClass?
    @ObservedObject var viewModel = ItunesItemDetailViewModel()
    @Environment(\.presentationMode) var presentationMode
    @State var showAlert: Bool = false

    private var onDeleteItem: (ItunesItem) -> Void

    init(item: Binding<ItunesItem>, onDeleteItem: @escaping (ItunesItem) -> Void) {
        self.onDeleteItem = onDeleteItem
        viewModel.item = item
        viewModel.getImage()
    }

    func renderUrlButton(_ text: String, _ url: String) -> some View {
        Button(action: {
            if let url = URL(string: url) {
                UIApplication.shared.open(url)
            }
        }) {
            Text(text).font(.largeTitle)
        }
    }

    func renderImage() -> some View {
        ItemImageView(
                imageData: self.$viewModel.imageData,
                kind: "song"
        ).frame(
                width: 200,
                height: 200
        ).padding(.top, 16)
    }

    func renderVertical(
            artist: String,
            artistViewUrl: String,
            collection: String,
            collectionViewUrl: String,
            previewUrl: String,
            track: String,
            genre: String,
            tPrice: String,
            cPrice: String
    ) -> some View {
        VStack(spacing: 8) {
            self.renderImage()
            self.renderUrlButton(artist, artistViewUrl)
            self.renderUrlButton(collection, collectionViewUrl)
            self.renderPlayButton()
            Text(track).font(.headline)
            Text(genre).font(.headline)
            Text(tPrice).font(.headline)
            Text(cPrice).font(.headline)
            Spacer()
        }
    }

    func renderPlayButton() -> some View {
        Button(action: {
            self.viewModel.play()
        }) {
            Image(systemName: self.$viewModel.playerIcon.wrappedValue).resizable().frame(width: 36, height: 36)
        }
    }

    func renderHorizontal(
            artist: String,
            artistViewUrl: String,
            collection: String,
            collectionViewUrl: String,
            previewUrl: String,
            track: String,
            genre: String,
            tPrice: String,
            cPrice: String
    ) -> some View {
        HStack(spacing: 8) {
            self.renderImage()
            VStack(alignment: .leading, spacing: 8) {
                HStack(alignment: .center, spacing: 8) {
                    self.renderUrlButton(artist, artistViewUrl)
                    Text(" - ").foregroundColor(.blue).font(.largeTitle)
                    self.renderUrlButton(collection, collectionViewUrl)
                    self.renderPlayButton()
                }
                Text(track).font(.headline)
                Text(genre).font(.headline)
                Text(tPrice).font(.headline)
                Text(cPrice).font(.headline)
            }
            Spacer()
        }
    }

    /**
     case largeTitle
     case title
     case headline
     case subheadline
     case body
     case callout
     case footnote
     case caption
     */
    var body: some View {
        let artist = viewModel.item?.wrappedValue.artistName ?? ""
        let artistViewUrl = viewModel.item?.wrappedValue.artistViewUrl ?? ""
        let collection = viewModel.item?.wrappedValue.collectionName ?? ""
        let collectionViewUrl = viewModel.item?.wrappedValue.collectionViewUrl ?? ""
        let previewUrl = viewModel.item?.wrappedValue.previewUrl ?? ""
        let track = viewModel.item?.wrappedValue.trackName ?? ""
        let genre = viewModel.item?.wrappedValue.primaryGenreName ?? ""
        let currency = "\(viewModel.item?.wrappedValue.currency ?? "")"
        let tPrice = "Track Price: \(viewModel.item?.wrappedValue.trackPrice ?? 0) \(currency)"
        let cPrice = "Collection Price: \(viewModel.item?.wrappedValue.collectionPrice ?? 0) \(currency)"
        return GeometryReader { geometry in
            ZStack(alignment: .top) {
                Group {
                    if self.verticalSizeClass == .regular && self.horizontalSizeClass == .compact {
                        // iPhone Portrait or iPad 1/3 split view for Multitasking for instance
                        self.renderVertical(
                                artist: artist,
                                artistViewUrl: artistViewUrl,
                                collection: collection,
                                collectionViewUrl: collectionViewUrl,
                                previewUrl: previewUrl,
                                track: track,
                                genre: genre,
                                tPrice: tPrice,
                                cPrice: cPrice
                        )
                    } else if self.verticalSizeClass == .compact && self.horizontalSizeClass == .compact {
                        // some "standard" iPhone Landscape (iPhone SE, X, XS, 7, 8, ...)
                        self.renderHorizontal(
                                artist: artist,
                                artistViewUrl: artistViewUrl,
                                collection: collection,
                                collectionViewUrl: collectionViewUrl,
                                previewUrl: previewUrl,
                                track: track,
                                genre: genre,
                                tPrice: tPrice,
                                cPrice: cPrice
                        )
                    } else if self.verticalSizeClass == .compact && self.horizontalSizeClass == .regular {
                        // some "bigger" iPhone Landscape (iPhone Xs Max, 6s Plus, 7 Plus, 8 Plus, ...)
                        self.renderHorizontal(
                                artist: artist,
                                artistViewUrl: artistViewUrl,
                                collection: collection,
                                collectionViewUrl: collectionViewUrl,
                                previewUrl: previewUrl,
                                track: track,
                                genre: genre,
                                tPrice: tPrice,
                                cPrice: cPrice
                        )
                    } else if self.verticalSizeClass == .regular && self.horizontalSizeClass == .regular {
                        // macOS or iPad without split view - no Multitasking
                        self.renderHorizontal(
                                artist: artist,
                                artistViewUrl: artistViewUrl,
                                collection: collection,
                                collectionViewUrl: collectionViewUrl,
                                previewUrl: previewUrl,
                                track: track,
                                genre: genre,
                                tPrice: tPrice,
                                cPrice: cPrice
                        )
                    }
                }
            }.navigationBarItems(
                    trailing: Button(action: {
                        self.showAlert = true
                    }, label: {
                        Image(systemName: "trash")
                    })
            ).alert(isPresented: self.$showAlert) {
                Alert(title: Text("Warning"),
                        message: Text("Are you sure"),
                        primaryButton: .cancel(Text("Cancel")) {
                            self.showAlert = false
                        },
                        secondaryButton: .default(Text("Delete")) {
                            self.viewModel.setItemDeleted()
                            if let item = self.viewModel.item?.wrappedValue {
                                self.onDeleteItem(item)
                            }
                            self.presentationMode.wrappedValue.dismiss()
                        }
                )
            }
        }.onAppear {
            self.viewModel.setItemVisited()
        }
    }
}
