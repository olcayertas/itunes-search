//
// Created by Olcay Ertaş on 6.06.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import SwiftUI

struct LoadingView<Content>: View where Content: View {

    @Binding var isShowing: Bool
    var content: () -> Content

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content().disabled(self.isShowing).blur(radius: self.isShowing ? 3 : 0)
                VStack {
                    Text("Loading...")
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                }.frame(width: 160, height: 160)
                        .background(Color.secondary.colorInvert())
                        .foregroundColor(Color.primary)
                        .cornerRadius(20)
                        .opacity(self.isShowing ? 1 : 0)
            }
        }
    }
}