//
// Created by Olcay Ertaş on 19.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct ItemImageView: View {

    var imageData: Binding<Data>
    var kind: String

    private func getPlaceHolderImage(for type: String) -> UIImage {
        //os_log("Placeholder image for %{public}@", log: .itemViewModel, type: .info, type)
        switch Kind(rawValue: type) {
        case .book:
            return UIImage(systemName: "book.fill")!
        case .album:
            return UIImage(systemName: "music.note.list")!
        case .pdf:
            return UIImage(systemName: "doc.text.fill")!
        case .podcast:
            return UIImage(systemName: "dot.radiowaves.left.and.right")!
        case .song:
            return UIImage(systemName: "music.mic")!
        case .artist:
            return UIImage(systemName: "person.circle.fill")!
        case .coachedAudio:
            return UIImage(systemName: "mic.fill")!
        case .featureMovie:
            return UIImage(systemName: "film.fill")!
        case .interactiveBooklet:
            return UIImage(systemName: "book.circle.fill")!
        case .musicVideo:
            return UIImage(systemName: "play.rectangle.fill")!
        case .podcastEpisode:
            return UIImage(systemName: "waveform.path")!
        case .softwarePackage:
            return UIImage(systemName: "keyboard")!
        case .tvEpisode:
            return UIImage(systemName: "tv.fill")!
        case .none:
            return UIImage(systemName: "questionmark")!
        }
    }

    var body: some View {
        Image(uiImage: UIImage(data: self.imageData.wrappedValue) ?? getPlaceHolderImage(for: kind)).resizable().renderingMode(.original)
    }
}