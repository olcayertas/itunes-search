//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import os.log

class ItunesSearchViewModel: ObservableObject {

    @Published var results: [ItunesItem] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var searchText: String = "" {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var isLoading = false {
        willSet {
            self.objectWillChange.send()
        }
    }

    private let historyManager = HistoryManager.sharedInstance

    var count: Int {
        results.count
    }

    private let apiClient = SearchApiClient()
    private var searchCancellable: AnyCancellable? = nil
    private var typeCancellable: AnyCancellable? = nil

    init() {
        typeCancellable = AnyCancellable(
                $searchText
                        .removeDuplicates()
                        .debounce(for: 0.8, scheduler: DispatchQueue.main)
                        .sink {
                            self.search(term: $0)
                        }
        )
    }

    deinit {
        searchCancellable?.cancel()
        typeCancellable?.cancel()
    }

    func persist() {
        os_log("persist", log: .searchViewModel, type: .info)
        historyManager.persist()
    }

    func isItemVisited(_ index: Int) -> Bool {
        historyManager.isVisited(id: results[index].trackId)
    }

    func search(term: String) {
        searchCancellable = apiClient.getResults(
                for: term,
                media: .music,
                entity: "song",
                country: "tr",
                limit: 100
        ).handleEvents { [weak self] _ in
            self?.isLoading = true
        }.sink(receiveCompletion: { [weak self] completion in
            self?.onComplete(completion)
        }, receiveValue: { [weak self] response in
            self?.onReceiveValue(response)
        })
    }

    private func onComplete(_ completion: Subscribers.Completion<Error>) {
        isLoading = false
        switch completion {
        case .finished:
            os_log("Finished receiving search results", log: .searchViewModel, type: .info)
        case .failure(let error):
            os_log("Error: %{public}@",
                    log: .searchViewModel,
                    type: .error,
                    error.localizedDescription
            )
        }
    }

    private func onReceiveValue(_ response: SearchResponse) {
        os_log("Received search results", log: .searchViewModel, type: .info)
        response.results?.forEach { item in
            item.isVisited = historyManager.isVisited(id: item.trackId)
            item.isDeleted = historyManager.isDeleted(id: item.trackId)
        }
        DispatchQueue.main.async { [weak self] in
            if let results = response.results {
                self?.results = results.filter {
                    $0.isDeleted == false
                }.map { item in
                    item.id = item.trackId ?? 0
                    return item
                }
            }
        }
    }
}
