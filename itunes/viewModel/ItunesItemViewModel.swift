//
// Created by Olcay Ertaş on 19.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import os.log
import AVFoundation

public enum Kind: String {
    case book
    case album
    case pdf
    case podcast
    case song
    case artist
    case coachedAudio = "coached-audio"
    case featureMovie = "feature-movie"
    case interactiveBooklet = "interactive-booklet"
    case musicVideo = "music-video"
    case podcastEpisode = "podcast-episode"
    case softwarePackage = "software-package"
    case tvEpisode = "tv-episode"
}

public class BaseItunesItemViewModel: ObservableObject {

    internal let apiClient = ImageApiClient()
    internal var task: Cancellable?

    internal var log: OSLog {
        OSLog.baseItemViewModel
    }

    let historyManager = HistoryManager.sharedInstance
    var item: Binding<ItunesItem>?

    @Published var imageData = Data() {
        willSet {
            self.objectWillChange.send()
        }
    }

    var isItemVisited: Bool {
        guard let id = item?.trackId.wrappedValue else {
            os_log("isItemVisited: Failed to get item id", log: log, type: .error)
            return false
        }
        return historyManager.isVisited(id: id)
    }

    var isItemDeleted: Bool {
        guard let id = item?.trackId.wrappedValue else {
            os_log("isItemDeleted: Failed to get item id", log: log, type: .error)
            return false
        }
        return historyManager.isDeleted(id: id)
    }

    private var player = AVPlayer()
    private let playIcon = "play.circle"
    private let pauseIcon = "pause.circle"

    @Published var playerIcon = "play.circle" {
        willSet {
            self.objectWillChange.send()
        }
    }

    private var observer: Any?

    func play() {
        if player.rate > 0 {
            playerIcon = playIcon
            player.pause()
        } else {
            if player.currentItem == nil, let previewUrl = item?.previewUrl.wrappedValue, let url = URL(string: previewUrl) {
                player.replaceCurrentItem(with: AVPlayerItem(url: url))
                if let observer = self.observer {
                    player.removeTimeObserver(observer)
                }
                observer = player.addBoundaryTimeObserver(
                        forTimes: [
                            NSValue(time: CMTime(seconds: 29, preferredTimescale: 1)), // for 30 seconds previews
                            NSValue(time: CMTime(seconds: 89, preferredTimescale: 1))  // for 90 seconds previews
                        ],
                        queue: .main
                ) { [weak self] in
                    self?.playerIcon = "play.circle"
                    if let observer = self?.observer {
                        self?.player.removeTimeObserver(observer)
                    }
                }
            }
            player.play()
            playerIcon = pauseIcon
        }
    }

    func setItemVisited() {
        guard let id = item?.trackId.wrappedValue else {
            os_log("setItemVisited: Failed to get item id", log: log, type: .error)
            return
        }
        os_log("setItemVisited: Item %{public}@ is visited", log: log, type: .error, "\(id)")
        item?.isVisited.wrappedValue = true
        historyManager.setVisited(id: id)
    }

    func setItemDeleted() {
        guard let id = item?.trackId.wrappedValue else {
            os_log("setItemDeleted: Failed to get item id", log: log, type: .error)
            return
        }
        os_log("setItemDeleted: Item %{public}@ is deleted", log: log, type: .error, "\(id)")
        item?.isDeleted.wrappedValue = true
        historyManager.setDeleted(id: id)
    }

    func getImage(url: String?) {
        task?.cancel()
        guard let url = url else {
            os_log("getImage: Failed to get image url", log: .itemViewModel, type: .info)
            return
        }
        if let data = UIImageCache.sharedInstance.imageData(for: url) {
            imageData = data
            os_log("getImage: Returned cached image data for %{public}@", log: log, type: .info, url)
            return
        }
        task = apiClient.getImage(url: url).sink(receiveCompletion: { [weak self, url] completion in
            self?.onCompletion(url, completion)
        }, receiveValue: { [weak self] data, _ in
            UIImageCache.sharedInstance.setImageData(data: data, url: url)
            self?.imageData = data
        })
    }

    func onCompletion(_ url: String, _ completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            os_log("getImage: Fetched image for %{public}@", log: log, type: .info, url)
        case .failure(let error):
            os_log("getImage: Failed to fetch image for %{public}@ : %{public}@",
                    log: .itemViewModel,
                    type: .error,
                    url,
                    error.localizedDescription
            )
        }
    }
}

public final class ItunesItemViewModel: BaseItunesItemViewModel {

    override var log: OSLog {
        .itemViewModel
    }

    var trackName: String {
        item?.trackName.wrappedValue ?? "???"
    }

    var artistName: String {
        item?.artistName.wrappedValue ?? "???"
    }

    var collectionName: String {
        item?.collectionName.wrappedValue ?? "???"
    }

    var kind: String {
        item?.kind.wrappedValue ?? "???"
    }

    func getImage() {
        getImage(url: item?.artworkUrl100.wrappedValue)
    }
}

public class ItunesItemDetailViewModel: BaseItunesItemViewModel {

    override var log: OSLog {
        .itemDetailViewModel
    }

    func getImage() {
        getImage(url: item?.artworkUrl100.wrappedValue)
    }
}
