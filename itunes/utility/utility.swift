//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import UIKit
import os.log
import SwiftUI
import Combine


fileprivate func loadArray<T: Decodable>(_ type: T.Type) -> [T] {
    loadArray(type, String(describing: T.self))
}


fileprivate func loadArray<T: Decodable>(_ type: T.Type, _ fileName: String) -> [T] {
    let path = Bundle.main.path(forResource: fileName, ofType: "json") ?? ""
    guard let data = FileManager.default.contents(atPath: path) else {
        os_log("Failed to load data from %{public}@", log: .loadArray, type: .info, fileName)
        return []
    }
    do {
        return try JSONDecoder().decode([T].self, from: data)
    } catch {
        os_log("Failed to decode data from %{public}@", log: .loadArray, type: .info, fileName)
        return []
    }
}


struct KeyboardAdaptive: ViewModifier {

    @State private var keyboardHeight: CGFloat = 0

    func body(content: Content) -> some View {
        content.padding(.bottom, keyboardHeight).onReceive(Publishers.keyboardHeight) {
            self.keyboardHeight = $0
        }
    }
}
