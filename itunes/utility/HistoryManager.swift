//
// Created by Olcay Ertaş on 22.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import os.log

public final class HistoryManager {

    private class ItemHistory: Codable, Hashable {

        static func ==(lhs: HistoryManager.ItemHistory, rhs: HistoryManager.ItemHistory) -> Bool {
            lhs.id == rhs.id
        }

        func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }

        init(id: Int?, isVisited: Bool?, isDeleted: Bool?) {
            self.id = id
            self.isVisited = isVisited
            self.isDeleted = isDeleted
        }

        init(id: Int?, isVisited: Bool?) {
            self.id = id
            self.isVisited = isVisited
        }

        init(id: Int?, isDeleted: Bool?) {
            self.id = id
            self.isDeleted = isDeleted
        }

        init(id: Int?) {
            self.id = id
        }

        var id: Int?
        var isVisited: Bool?
        var isDeleted: Bool?
    }

    private class HistoryData: Codable {
        var data = Set<ItemHistory>()
    }

    private static let key = "com.itunes.search.history"

    private var historyData: HistoryData

    public static let sharedInstance = HistoryManager()

    private init() {
        do {
            if let data = UserDefaults.standard.data(forKey: HistoryManager.key) {
                historyData = try JSONDecoder().decode(HistoryData.self, from: data)
            } else {
                os_log("init: Failed to get history data from UserDefaults", log: .historyData, type: .info)
                historyData = HistoryData()
            }
        } catch (let error) {
            os_log("init: %{public}@", log: .historyData, type: .error, error.localizedDescription)
            historyData = HistoryData()
        }
    }

    public func persist() {
        do {
            let data = try JSONEncoder().encode(historyData)
            UserDefaults.standard.setValue(data, forKey: HistoryManager.key)
            os_log("persist: Saved history", log: .historyData, type: .info)
        } catch (let error) {
            os_log("persist: %{public}@", log: .historyData, type: .error, error.localizedDescription)
        }
    }

    func setVisited(id: Int?) {
        if let id = id {
            if let item = historyData.data.remove(ItemHistory(id: id)) {
                item.isVisited = true
                historyData.data.insert(item)
            } else {
                historyData.data.insert(ItemHistory(id: id, isVisited: true))
            }
        }
    }

    func isVisited(id: Int?) -> Bool {
        if let index = historyData.data.firstIndex(of: ItemHistory(id: id)) {
            return historyData.data[index].isVisited ?? false
        } else {
            return false
        }
    }

    func setDeleted(id: Int?) {
        if let id = id {
            if let item = historyData.data.remove(ItemHistory(id: id)) {
                item.isDeleted = true
                historyData.data.insert(item)
            } else {
                historyData.data.insert(ItemHistory(id: id, isDeleted: true))
            }
        }
    }

    func isDeleted(id: Int?) -> Bool {
        if let index = historyData.data.firstIndex(of: ItemHistory(id: id)) {
            return historyData.data[index].isDeleted ?? false
        } else {
            return false
        }
    }
}
