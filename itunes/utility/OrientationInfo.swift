//
// Created by Olcay Ertaş on 22.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import UIKit

final class OrientationInfo: ObservableObject {

    public enum Orientation {
        case portrait
        case landscape
    }

    @Published var orientation: Orientation {
        didSet {
            self.objectWillChange.send()
        }
    }

    private var _observer: NSObjectProtocol?

    init() {
        // TODO: It is not working when device is landscape mode initially.
        self.orientation = UIScreen.main.bounds.height < UIScreen.main.bounds.width ? .landscape : .portrait
        // unowned self because we unregister before self becomes invalid
        _observer = NotificationCenter.default.addObserver(
                forName: UIDevice.orientationDidChangeNotification,
                object: nil,
                queue: nil
        ) { [unowned self] note in
            guard let device = note.object as? UIDevice else {
                return
            }
            self.orientation = device.orientation.isLandscape ? .landscape : .portrait
        }
    }

    deinit {
        if let observer = _observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}
