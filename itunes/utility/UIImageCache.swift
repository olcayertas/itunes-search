//
// Created by Olcay Ertaş on 22.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

public final class UIImageCache {

    var imageSet = [String: Data]()

    public static let sharedInstance = UIImageCache()

    func setImageData(data: Data, url: String) {
        UserDefaults.standard.set(data, forKey: url)
        imageSet[url] = data
    }

    func loadImage(for url: String) {
        imageSet[url] = UserDefaults.standard.data(forKey: url)
    }

    func imageData(for url: String) -> Data? {
        // Try to load data if it is not loaded yet
        if imageSet[url] == nil {
            UIImageCache.sharedInstance.loadImage(for: url)
        }
        return imageSet[url]
    }
}
