//
// Created by Olcay Ertaş on 17.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

public class ItunesItem: Codable, Hashable, Identifiable {

    public static func ==(lhs: ItunesItem, rhs: ItunesItem) -> Bool {
        lhs.trackId == rhs.trackId
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(trackId)
    }

    public var id: Int? = 0
    public var isVisited: Bool?
    public var isDeleted: Bool?
    public var artistId : Int?
    public var artistName : String?
    public var artistViewUrl : String?
    public var artworkUrl100 : String?
    public var artworkUrl30 : String?
    public var artworkUrl60 : String?
    public var collectionCensoredName : String?
    public var collectionExplicitness : String?
    public var collectionId : Int?
    public var collectionName : String?
    public var collectionPrice : Float?
    public var collectionViewUrl : String?
    public var country : String?
    public var currency : String?
    public var discCount : Int?
    public var discNumber : Int?
    public var isStreamable : Bool?
    public var kind : String?
    public var previewUrl : String?
    public var primaryGenreName : String?
    public var releaseDate : String?
    public var trackCensoredName : String?
    public var trackCount : Int?
    public var trackExplicitness : String?
    public var trackId : Int?
    public var trackName : String?
    public var trackNumber : Int?
    public var trackPrice : Float?
    public var trackTimeMillis : Int?
    public var trackViewUrl : String?
    public var wrapperType : String?
}
