//
// Created by Olcay Ertaş on 17.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

struct SearchResponse: Codable {
    var resultCount: Int?
    var results: [ItunesItem]?
}
