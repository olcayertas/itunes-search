# iTunes Search - SwiftUI & MVVM & Combine
> Simple iTunes search application using SwiftUI - MVVM - Combine.

[![Swift Version][swift-image]][swift-url]
[![Build Status][travis-image]][travis-url]
[![License][license-image]][license-url]
[![Platform](https://img.shields.io/badge/Platform-iOS-green)]()
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)]()

This simple app can do followings:

## Features

- [x] Search using iTunes Search API.
- [x] List items in both portrait and landscape modes.
- [x] In landscape mode items listed in two columns.
- [x] Remembers visited items and shows them with gray background.
- [x] Can mark items deleted and exclude items that marked as deleted from list.
- [x] List items show collection image, artist name, track name and collection name.
- [x] List items can also play preview of the song.
- [x] Detail screen also shows track and collection prices.
- [x] Detail screen can direct user to artist or collection pages.
- [x] Detail screen supports portrait and landscape modes with different layouts.

## Application architecture

This application use `MVVM` architecture with Apples's reactive programming framework `Combine`. This makes possible to develop this application without any third party library. Using `MVVM` with reactive programming makes it really easy to develop large scale applications. Using `MVVM` or `VIPER` like architectures also makes it east to test the application features.

## Persisting visit and delete history

This application use `UserDefaults` based approach to store visit and delete history of the items. Visited or deleted item data is stored in a `Codable` object. This object is persisted when the application enters background. When application is started this data is read from UserDefaults. All search results are filtered against this data before showing them to user.

## Why did I use SwiftUI?

I have already pretty good experience with `UIKit` for developing user interfaces. I prefer to use `SwiftUI` when it is possible so I can advance my knowledge about it. This makes me stay at the bleeding edge of the technology. Also SwiftUI is the feature for iOS. Apple announced a lot of improvements to SwiftUI at the `WWDC-2020`.

## Does this application ready to submit to AppStore? 

No, It is not ready. To make it ready it should have:

 - Analytics integration to gain insight about what users doing with this app.
 - Notification support to get user attention.
 - A decent app theme and an app icons.
 - More UI options to use available filtering options of the iTunes search API.

![Screenshot](images/list-portrait.png)

![Screenshot](images/detail-portrait.png)

## Requirements

- iOS 13.0+
- Xcode 11.5

## Installation

#### Manually
1. Download and open in Xcode.
2. No third party library is required.

## Contribute

I would love you for the contribution, check the ``LICENSE`` file for more info.

## Meta

Olcay Ertaş – [@Linkedin](https://www.linkedin.com/in/olcayertas/) – olcayertas@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://github.com/olcayertas](https://github.com/olcayertas)

[swift-image]:https://img.shields.io/badge/swift-5.0-orange.svg
[swift-url]: https://swift.org/
[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[codebeat-image]: https://codebeat.co/badges/c19b47ea-2f9d-45df-8458-b2d952fe9dad
[codebeat-url]: https://codebeat.co/projects/github-com-vsouza-awesomeios-com
