//
//  itunesTests.swift
//  itunesTests
//
//  Created by Olcay Ertaş on 17.07.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import XCTest
import Combine
import Foundation
@testable import itunes

class itunesTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put tear down code here. This method is called after the invocation of each test method in the class.
        cancelBag.forEach { cancellable in
            cancellable.cancel()
        }
    }

    func testSearch() throws {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "itunes.apple.com"
        components.path = "/search"
        components.queryItems = [
            URLQueryItem(name: "term", value: "tarkan"),
        ]
        guard let url = components.url else {
            XCTAssertThrowsError("Failed to create URL!")
            return
        }
        let promise = XCTestExpectation(description: "Search")
        SearchApiClient()
                .fetch(url: url)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished: promise.fulfill()
                    case .failure: XCTFail()
                    }
                }, receiveValue: { (data, code) in
                    XCTAssertNotNil(data)
                    XCTAssertEqual(code, 200)
                }).store(in: &cancelBag)

        wait(for: [promise], timeout: 5.0)
    }

    private var cancelBag = Set<AnyCancellable>()

    func testImageDataFetch() throws {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "is3-ssl.mzstatic.com"
        components.path = "/image/thumb/Music2/v4/3a/3e/cf/3a3ecf03-8b3d-eab7-11b1-b9d1205aae73/source/100x100bb.jpg"
        guard let url = components.url else {
            XCTAssertThrowsError("Failed to create URL!")
            return
        }
        let promise = XCTestExpectation(description: "Download")
        ImageApiClient()
                .fetch(url: url)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished: promise.fulfill()
                    case .failure: XCTFail()
                    }
                }, receiveValue: { (data, code) in
                    XCTAssertNotNil(data)
                    XCTAssertEqual(code, 200)
                }).store(in: &cancelBag)
        wait(for: [promise], timeout: 5.0)
    }

    var viewModel: ItunesSearchViewModel?

    func testSearchViewModelDebounce() throws {
        viewModel = ItunesSearchViewModel()
        viewModel?.searchText = "tarkan"
        guard let viewModel = viewModel else {
            XCTAssertThrowsError("Failed to get view model!")
            return
        }
        XCTAssertFalse(viewModel.isLoading)
    }

    func testSearchViewModelDebounce2() throws {
        viewModel = ItunesSearchViewModel()
        let promise = XCTestExpectation(description: "Debounce")
        viewModel?.searchText = "tarkan"
        // debounce time is 0.8, this should yield true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [viewModel] in
            XCTAssertTrue(viewModel!.isLoading)
            promise.fulfill()
        }
        wait(for: [promise], timeout: 5.0)
    }

    func testSearchViewModelSearch() {
        viewModel = ItunesSearchViewModel()
        let promise = XCTestExpectation(description: "Search")
        viewModel?.searchText = "tarkan"
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [viewModel] in
            guard let count = viewModel?.results.count else {
                XCTAssertThrowsError("Failed to get response count!")
                return
            }
            XCTAssertTrue(count > 0)
            promise.fulfill()
        }
        wait(for: [promise], timeout: 5.0)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
